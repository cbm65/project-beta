import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()
from service_rest.models import AutomobileVO, CustomerVO

def get_automobiles():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    for automobile in content["automobiles"]:
        AutomobileVO.objects.update_or_create(
            import_href=automobile["href"],
            defaults={"vin": automobile["vin"]},
        )

def get_customers():
    response = requests.get("http://sales-api:8000/api/customers/")
    content = json.loads(response.content)
    for customer in content["customers"]:
         CustomerVO.objects.update_or_create(
             import_id=customer["id"],
             defaults={"name": customer["name"]},
         )


def poll():
    while True:
        print('Service poller polling for data')
        try:
            get_automobiles(),
            get_customers(),
            print("Poller is working!")
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
