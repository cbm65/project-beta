from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)


class CustomerVO(models.Model):
    name = models.CharField(max_length=200, unique=True)
    import_id = models.PositiveIntegerField()


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name

class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=50)
    date = models.CharField(max_length=50)
    time = models.CharField(max_length=50)
    vip = models.BooleanField(default=False)
    reason = models.CharField(max_length=200)
    has_finished = models.BooleanField(default=False)
    cancelled = models.BooleanField(default=False)
    customer = models.CharField(max_length=50)
    technician = models.ForeignKey(
        Technician,
        related_name="technicians",
        on_delete=models.PROTECT,
    )

    # customer = models.ForeignKey(
    #     CustomerVO,
    #     related_name="customers",
    #     on_delete=models.PROTECT
    # )

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.customer} service appointment. VIP = {self.vip}"

    def finished(self):
        self.has_finished = not self.has_finished
        self.save()

    def app_status(self):
        self.cancelled = not self.cancelled
        self.save()

    def is_vip(self, *args, **kwargs):
        has_dealership_car = AutomobileVO.objects.filter(vin=self.vin)
        self.vip = len(has_dealership_car) > 0
        super().save(*args, **kwargs)

    class Meta:
        ordering = ("date", "customer", "reason")
