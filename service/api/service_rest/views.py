from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, ServiceAppointment, CustomerVO
from django.http import JsonResponse

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [ "import_href", "vin", ]

# class CustomerVOEncoder(ModelEncoder):
#     model = CustomerVO
#     properties = [ "import_id", "name",]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [ "id", "name", "employee_number",]

class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = ["id", "vin", "customer", "date", "time", "technician",
    "vip", "reason", "cancelled", "has_finished", ]
    encoders={
        "technician": TechnicianEncoder(),
        # "customer": CustomerVOEncoder(),

    }

class HistoryEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = ["id", "vin", "customer", "date", "time", "technician", "reason",]

@require_http_methods(["GET", "POST"])
def api_technicians_list(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            TechnicianEncoder,
            safe = False,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                TechnicianEncoder,
                safe = False,
            )
        except:
            return JsonResponse(
                {"message": "Unable to create technician"},
                status = 400,
            )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_technician(request):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                TechnicianEncoder,
                safe = False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Incorrect technician employee number provided."},
                status = 400
            )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            TechnicianEncoder,
            safe = False,
        )

@require_http_methods(["GET", "POST"])
def api_show_history(request):
    if request.method == "GET":
        history = ServiceAppointment.objects.all()
        if history == []:
            return JsonResponse(
                {"message": "Incorrect VIN"},
                status = 404,
                safe = False,
            )
        else:
            print("This is the history", history)
            return JsonResponse(
                history,
                HistoryEncoder,
                safe=False
            )

@require_http_methods(["GET", "POST"])
def api_show_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician_id = content["technician_id"]
            technician = Technician.objects.get(pk=technician_id)
            content["technician"] = technician
            # customer_id = content["customer"]
            # customer = CustomerVO.objects.get(import_id=customer_id)
            # content["customer"] = customer
            # automobile_href = content["automobile"]
            # automobile = AutomobileVO.objects.get(import_href=automobile_href)
            # content["automobile"] = automobile
            appointment = ServiceAppointment.objects.create(**content)

            return JsonResponse(
                appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the appointment"}
            )
            response.status_code = 400
            return response
