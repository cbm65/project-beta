from django.contrib import admin
from .models import AutomobileVO, Technician, ServiceAppointment, CustomerVO

admin.site.register(AutomobileVO)
admin.site.register(Technician)
admin.site.register(ServiceAppointment)
admin.site.register(CustomerVO)
