from django.urls import path
from .views import (
    api_show_history,
    api_show_appointments,
    api_technicians_list,
    api_show_technician,
)

urlpatterns = [

    path("appointments/", api_show_appointments, name="api_show_appointments"),
    path("appointments/<int:pk>/", api_show_appointments, name="api_show_appointments"),
    path("technicians/", api_technicians_list, name="api_technicians_list"),
    path("technicians/<int:pk>/", api_show_technician, name ="api_show_technician"),
]
