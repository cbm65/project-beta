# CarCar

Team:

* Conor - Sales
* Alissa - Service


## How to Run this Application

1. Clone project into local directory
2. Run the following terminal commands:
    - docker volume create beta-data
    - docker-compose build
    - docker-compose up
3. Go to http://localhost:3000/ in your browser
4. Enjoy!

## Applicaiton Diagram

image.png


## CRUD Routes, API Documentation
Appointment Service:
Localhost, Port 8080

Sales Service:
Localhost, Port 8090

Inventory Service:
Localhost, Port 8100


SALES SERVICE

Representatives:

POST request to /api/representatives/
status code:200
{
	"name": "Ash Simms",
	"employee_number": "5"
}

GET request to /api/representatives/ returns:
status code:200
{
	"representatives": [
		{
			"id": 1,
			"name": "Luke Skywalker",
			"employee_number": 1
		}
    ]
}

GET request to /api/representatives/<int:pk>/" returns:
status code:200
{
	"id": 3,
	"name": "Ash Simms",
	"employee_number": 5
}

Customers:

POST request to /api/customers/
status code:200
{
	"name": "Dan Meyers",
	"address": "8976 Beer Dr",
	"phone_number": "098-208-0867"
}

GET request to /api/customers/ returns:
status code:200
{
	"customers": [
		{
			"id": 1,
			"name": "Nick Jones",
			"phone_number": "303-986-7412",
			"address": "2058 S Corona St"
		}
    ]
}

GET request to /api/customers/<int:pk>/" returns:
status code:200
{
	"id": 2,
	"name": "Parker Peter",
	"phone_number": "365-987-9614",
	"address": "6419 S Newport Ct"
}

Sales:

POST request to /api/sales/
status code:200
{
	"price": 10000,
	"automobile": "/api/automobiles/983CCMZ7PLAN19417/",
	"representative_id": 1,
	"customer_id": 1
}

GET request to /api/sales/ returns:
status code:200
{
	"sales": [
		{
        "id": 18,
        "price": 10000,
        "automobile": {
            "vin": "1C3CCMZ72AN1TY174",
            "import_href": "/api/automobiles/1C3CCMZ72AN1TY174/"
        },
        "customer": {
            "id": 1,
            "name": "Pearl Smith",
            "phone_number": "626-985-8520",
            "address": "2637 Elm St"
        },
        "representative": {
            "id": 1,
            "name": "Conor McCabe",
            "employee_number": 1
        }
    ]
}

GET request to /api/sales/<int:pk>/" returns:
status code:200
{
	"id": 3,
	"price": 10000,
	"automobile": {
		"vin": "983CCMZ7PLAN19417",
		"import_href": "/api/automobiles/983CCMZ7PLAN19417/"
	},
	"customer": {
		"id": 1,
		"name": "Nick Jones",
		"phone_number": "303-986-7412",
		"address": "2058 S Corona St"
	},
	"representative": {
		"id": 1,
		"name": "Luke Skywalker",
		"employee_number": 1
	}
}


APPOINTMENTS SERVICE

Technicians:

POST request to /api/technicians/
status code:200
{
	"name": "Conor McCabe",
	"employee_number": "1"
}

GET request to /api/technicians/ returns:
status code:200
{
	"technicians": [
		{
			"href": "/api/technicians/1/",
			"id": 1,
			"name": "Conor McCabe",
			"employee_number": 1
		}
	]
}

GET request to /api/technicians/<int:pk>/" returns:
status code:200
{
    "href": "/api/technicians/1/",
    "id": 1,
    "name": "Conor McCabe",
    "employee_number": 1
}

Appointments:

POST request to /api/appointments/
status code:200
{
    "vin": "1C3CCMZ72AN1TY174",
    "customer": "Conor Smith",
    "date": "2023-01-01",
    "time": "16:00",
    "technician": 1
    "reason": "test",
}

GET request to /api/appointments/ returns:
status code:200
{
	"appointments": [
		{
			"id": 1,
			"vin": "1C3CCMZ72AN1TY174",
			"customer": "Conor Smith",
			"date": "2023-01-01",
			"time": "16:00",
			"technician": {
				"href": "/api/technicians/1/",
				"id": 1,
				"name": "Conor McCabe",
				"employee_number": 1
			},
			"vip": false,
			"reason": "test",
			"cancelled": false,
			"has_finished": false
		}
    ]
}

GET request to /api/appointments/<int:pk>/" returns:
status code:200
{
    "id": 1,
    "vin": "1C3CCMZ72AN1TY174",
    "customer": "Conor Smith",
    "date": "2023-01-01",
    "time": "16:00",
    "technician": {
        "href": "/api/technicians/1/",
        "id": 1,
        "name": "Conor McCabe",
        "employee_number": 1
    },
    "vip": false,
    "reason": "test",
    "cancelled": false,
    "has_finished": false
}



## Design

CarCar is designed using two microservices that both poll automobile data from the inventory service to accomplish their respective responsibilities.

## Service microservice

Models:
Technician
Appointments
AutomobileVO (data polled from Automobile class in Inventory)

Service uses the technician model in Django backend and a customized form in React frontend to allow users to add new technicians

After a technician is created using the form, a service appointment can be added assigning a technician and entering relevant appointment data in the customized React form (date, time, VIN, etc)

Sales sppointments can then be designated as completed or cancelled, at which they are recorded in appointment history and can be viewed by searching using the relevant vehicles VIN number

## Sales microservice

Models:
Representative
Customer
Sale
AutomobileVO (data polled from Automobile class in Inventory)

Sales uses the Representative and Customer models in Django backend and customized forms in React frontend to allow users to add new sales representatives and customers

After representatives and customers are created using the forms, a sale can be added assigning a customer, representative and vehicle to a price using the customized React form

After sales are recorded, they can be viewed in the Sales History section and can also be sorted by sales representative. After a vehicle is recorded as sold, it can no longer be recorded in another sale unless the initial sale is deleted
