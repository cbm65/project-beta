import React, {useEffect, useState} from 'react';



function RepresentativeForm({ getRepresentatives}) {

// Set the useState hook to store "name" in the component's state,
// with a default initial value of an empty string.

    const [name, setName] = useState('');
    const [employee_number, setEmployeeNumber] = useState('');
    const [formSubmitted, setformSubmitted] = useState(false);


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }

    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
      }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.employee_number = employee_number;


        const representativeUrl = 'http://localhost:8090/api/representatives/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(representativeUrl, fetchConfig);
        if (response.ok) {
          const newRepresentative = await response.json();

          setName('');
          setEmployeeNumber('');
          setformSubmitted(true);

          return getRepresentatives();
        }
      }

      let messageClasses = 'alert alert-success d-none mb-0';
      let formClasses = '';

      if (formSubmitted) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
      }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new sales representative</h1>
            <form className={formClasses} onSubmit={handleSubmit} id="create-representative-form">
                <div className="form-floating mb-3">
                    <input onChange = {handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange = {handleEmployeeNumberChange} value={employee_number} placeholder="employee_number" required type="number" employee_number="employee_number" id="employee_number" className="form-control"/>
                    <label htmlFor="employee_number">employee number</label>
                </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={messageClasses} id="success-message">
                  Sales Rep Added Successfully!
                </div>
          </div>
        </div>
      </div>
    );
  }


export default RepresentativeForm;
