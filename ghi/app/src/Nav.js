import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <NavLink className="navbar-brand" to="manufacturers/">List Manufacturers</NavLink>
        <NavLink className="navbar-brand" to="manufacturers/new/">Create a Manufacturer</NavLink>
        <NavLink className="navbar-brand" to="models/">List Models</NavLink>
        <NavLink className="navbar-brand" to="models/new/">Create a Model</NavLink>
        <NavLink className="navbar-brand" to="automobiles/">List Automobiles</NavLink>
        <NavLink className="navbar-brand" to="automobiles/new/">Create an Automobile</NavLink>
        <NavLink className="navbar-brand" to="representatives/new/">Add a Sales Rep</NavLink>
        <NavLink className="navbar-brand" to="customers/new/">Add a Potential Customer</NavLink>
        <NavLink className="navbar-brand" to="sales/new/">Record a New Sale</NavLink>
        <NavLink className="navbar-brand" to="sales/">Sales History</NavLink>
        <NavLink className="navbar-brand" to="appointments/history">Service History</NavLink>
        <NavLink className="navbar-brand" to="appointments/">Service Appointments</NavLink>
        <NavLink className="navbar-brand" to="appointments/new">Create Service Appointment</NavLink>
        <NavLink className="navbar-brand" to="technicians/new">Add a Technician</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
