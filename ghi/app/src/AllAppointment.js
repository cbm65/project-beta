import React from "react"


function AppointmentList({ appointmentlist}) {

  if (appointmentlist === undefined) {
    return null;
  }
  return (
  <div className="container">
    <h1>Service Appointments</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>VIN</th>
          <th>Time</th>
          <th>Date</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>
      <tbody>
        {appointmentlist.map((appointment) => {
          return (
            <tr key={appointment.name}>
              <td>{ appointment.automobile.vin }</td>
              <td>{ appointment.time }</td>
              <td>{ appointment.date }</td>
              <td>{ appointment.technician }</td>
              <td>{ appointment.reason }</td>
              <td>
                <button value={appointment.id} onClick={cancelAppointment} type="button" className="btn btn-danger">
                  Cancel
                </button>
              </td>
              <td>
                <button value={appointment.id} onClick={finishAppointment} type="button" className="btn btn-danger">
                  Finished
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </div>
  );
}
export default AppointmentList;