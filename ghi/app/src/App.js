import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import { useState, useEffect } from "react";
import ManufacturerList from './ListManufacturer';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ListModel';
import ModelForm from './ModelForm';
import AutomobileList from './ListAutomobile';
import AutomobileForm from './AutomobileForm';
import RepresentativeForm from './RepresentativeForm';
import CustomerForm from './CustomerForm';
import SaleForm from './SaleForm';
import SaleList from './ListSales';
import TechnicianForm from './TechnicianForm';
import NewServiceForm from './ServiceForm';
import AppointmentList from './ListAppointments';
import ServiceHistory from './ServiceHistory';

function App() {



  const [manufacturers, setManufacturers] = useState([])

  const getManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const manufacturers = data.manufacturers
      setManufacturers(manufacturers)
    }
  }

  const [models, setModels] = useState([])

  const getModels = async () => {
    const url = 'http://localhost:8100/api/models/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const models = data.models
      setModels(models)
    }
  }

  const [automobiles, setAutomobiles] = useState([])

  const getAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const automobiles = data.automobiles
      setAutomobiles(automobiles)
    }
  }

  const [representatives, setRepresentatives] = useState([])

  const getRepresentatives = async () => {
    const url = 'http://localhost:8090/api/representatives/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const representatives = data.representatives
      setRepresentatives(representatives)
    }
  }

  const [customers, setCustomers] = useState([])

  const getCustomers = async () => {
    const url = 'http://localhost:8090/api/customers/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const customers = data.customers
      setCustomers(customers)
    }
  }

  const [sales, setSales] = useState([])

  const getSales = async () => {
    const url = 'http://localhost:8090/api/sales/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const sales = data.sales
      setSales(sales)
    }
  }

  const [technicians, setTechnicians] = useState([])

  const getTechnicians = async () => {
    const url = 'http://localhost:8080/api/technicians/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const technicians = data.technicians
      setTechnicians(technicians)
    }
  }

  const [appointments, setAppointments] = useState([])

  const getAppointments = async () => {
    const url = 'http://localhost:8080/api/appointments/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const appointments = data.appointments
      setAppointments(appointments)
    }
  }


  useEffect(() => {

    getManufacturers();
    getModels();
    getAutomobiles();
    getRepresentatives();
    getCustomers();
    getSales();
    getTechnicians();
    getAppointments();

  }, []);



  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="manufacturers">
            <Route index element={<ManufacturerList manufacturers={manufacturers} getManufacturers={getManufacturers} />} />
            <Route path="new" element={<ManufacturerForm getManufacturers={getManufacturers}/>} />
          </Route>

          <Route path="models">
            <Route index element={<ModelList models={models} getModels={getModels} />} />
            <Route path="new" element={<ModelForm getModels={getModels}/>} />
          </Route>

          <Route path="automobiles">
            <Route index element={<AutomobileList automobiles={automobiles} getAutomobiles={getAutomobiles} />} />
            <Route path="new" element={<AutomobileForm getAutomobiles={getAutomobiles}/>} />
          </Route>

          <Route path="representatives">
            <Route path="new" element={<RepresentativeForm getRepresentatives={getRepresentatives}/>} />
          </Route>

          <Route path="customers">
            <Route path="new" element={<CustomerForm getCustomers={getCustomers}/>} />
          </Route>


          <Route path="sales">
            <Route index element={<SaleList sales={sales} getSales={getSales} />} />
            <Route path="new" element={<SaleForm getSales={getSales}/>} />
          </Route>

          <Route path="technicians">
            <Route path="new" element={<TechnicianForm getTechnicians={getTechnicians}/>} />
          </Route>

          <Route path="appointments">
            <Route index element={<AppointmentList appointments={appointments} getAppointments={getAppointments} />} />
            <Route path="new" element={<NewServiceForm getAppointments={getAppointments}/>} />
            <Route path="history" element={<ServiceHistory appointments={appointments} setAppointments={setAppointments}/>}/>
          </Route>


        </Routes>
      </div>
    </BrowserRouter>
  );
  }
export default App;
