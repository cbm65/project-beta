import React, {useEffect, useState} from 'react';



function AutomobileForm({ getAutomobiles}) {

// Set the useState hook to store "name" in the component's state,
// with a default initial value of an empty string.

    const [vin, setVin] = useState('');
    const [color, setColor] = useState("");
    const [year, setYear] = useState('');
    const [model, setModel] = useState("");
    const [models, setModels] = useState([]);
    const [formSubmitted, setformSubmitted] = useState(false);


    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
      }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
      }
    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
      }
    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
      }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = vin;
        data.color = color;
        data.year = year;
        data.model_id = model;


        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
          const newAutomobile = await response.json();


          setVin('');
          setColor('');
          setYear('');
          setModel('');
          setformSubmitted(true);

          return getAutomobiles();
        }
      }



    const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models)
    }
  }
  useEffect(() => {
    fetchData();
  }, []);

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';

  if (formSubmitted) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new automobile</h1>
            <form className={formClasses} onSubmit={handleSubmit} id="create-automobile-form">
              <div className="form-floating mb-3">
                <input onChange = {handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">vin</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange = {handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">color</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange = {handleYearChange} value={year} placeholder="year" required type="number" name="year" id="year" className="form-control"/>
                <label htmlFor="year">year</label>
              </div>

              <div className="mb-3">
                <select onChange = {handleModelChange} value={model} required id="model" name="model" className="form-select">
                  <option value="">choose a model</option>
                  {models.map(model => {
                    return (
                    <option key={model.id} value={model.id}>
                    {model.name}
                    </option>
                    );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={messageClasses} id="success-message">
                  Automobile Created Successfully!
                </div>
          </div>
        </div>
      </div>
    );
  }


export default AutomobileForm;
