import React, {useEffect, useState} from "react"
import { Link } from "react-router-dom";


function SaleList({ sales, getSales}) {


    const [representative, setRepresentative] = useState("");
    const [representatives, setRepresentatives] = useState([]);

    let newSales = []

    if (representative === "")

    newSales = sales

    for (let i = 0; i < sales.length; i++)

        if (sales[i]["representative"]["name"] === representative)

            newSales.push(sales[i])


    const handleRepresentativeChange = (event) => {
        const value = event.target.value;
        setRepresentative(value);
      }


    const fetchData = async () => {
    const url = 'http://localhost:8090/api/representatives/';


    const response = await fetch(url);


    if (response.ok) {
        const data = await response.json();
        setRepresentatives(data.representatives)
    }

  }

    useEffect(() => {
        fetchData();
    }, []);


    const deleteSale = async (id) => {
        fetch(`http://localhost:8090/api/sales/${id}/`, {
        method:"delete",
        })
        .then(() => {
        return getSales()
        })
    }

    if (sales === undefined) {
        return null
    }


  return (
  <>
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Filter by Sales Rep</h1>
            <form id="create-sale-form">

                <div className="mb-3">
                    <select onChange = {handleRepresentativeChange} value={representative} required id="representative" name="representative" className="form-select">
                    <option value="">choose a representative</option>
                    {representatives.map(representative => {
                        return (
                        <option key={representative.id} value={representative.name}>
                        {representative.name}
                        </option>
                        );
                        })}
                    </select>
                </div>

            </form>
          </div>
        </div>
      </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Sales Rep</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Sales Price</th>
        </tr>
      </thead>
      <tbody>
        {newSales.map((sale) => {
          return (
            <tr key={sale.id} >
              <td>{ sale.representative.name } </td>
              <td>{ sale.customer.name }</td>
              <td>{ sale.automobile.vin }</td>
              <td>{ sale.price }</td>
              <td>
                  <button type="button" value={sale.id} onClick={() => deleteSale(sale.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}

export default SaleList;
