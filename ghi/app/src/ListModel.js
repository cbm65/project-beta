import React, {useEffect, useState} from "react"
import { Link } from "react-router-dom";


function ModelList({ models, getModels}) {


  const deleteModel = async (id) => {
    fetch(`http://localhost:8100/api/models/${id}/`, {
      method:"delete",
    })
    .then(() => {
      return getModels()
    })
  }

  if (models === undefined) {
    return null
  }


  return (
  <>
    <table className="table table-striped">
      <thead>
        <tr>

          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>

        </tr>
      </thead>
      <tbody>
        {models.map((model) => {
          return (
            <tr key={model.id}>
              <td>{ model.name }</td>
              <td>{ model.manufacturer.name }</td>
              <td><img src={model.picture_url} height="100" width="150"></img></td>

              <td>
                  <button type="button" value={model.id} onClick={() => deleteModel(model.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}

export default ModelList;
