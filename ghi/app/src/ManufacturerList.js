import React, {useEffect, useState} from "react"
import { Link } from "react-router-dom";
function ManufacturerList({ manufacturers, getManufacturers}) {
  const deleteManufacturer = async (id) => {
    fetch(`http://localhost:8100/api/manufacturers/${id}/`, {
      method:"delete",
    })
    .then(() => {
      return getManufacturers()
    })
  }
  if (manufacturers === undefined) {
    return null
  }
  return (
  <>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {manufacturers.map((manufacturer) => {
          return (
            <tr key={manufacturer.id}>
              <td>{ manufacturer.name }</td>
              <td>
                  <button type="button" value={manufacturer.id} onClick={() => deleteManufacturer(manufacturer.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}
export default ManufacturerList;
