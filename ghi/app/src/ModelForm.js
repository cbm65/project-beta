import React, {useEffect, useState} from 'react';



function ModelForm({ getModels}) {

// Set the useState hook to store "name" in the component's state,
// with a default initial value of an empty string.

    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState("");
    const [manufacturer, setManufacturer] = useState("");
    const [manufacturers, setManufacturers] = useState([]);
    const [formSubmitted, setformSubmitted] = useState(false);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
      }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
      }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturer;


        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
          const newModel = await response.json();

          setName('');
          setPictureUrl('');
          setManufacturer('');
          setformSubmitted(true);

          return getModels();
        }
      }



    const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';

  if (formSubmitted) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new model</h1>
            <form className={formClasses} onSubmit={handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input onChange = {handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {handlePictureUrlChange} value={pictureUrl} placeholder="picture url" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="Maximum presentations">picture url</label>
              </div>

              <div className="mb-3">
                <select onChange = {handleManufacturerChange} value={manufacturer} required id="manufacturer" name="manufacturer" className="form-select">
                  <option value="">choose a manufacturer</option>
                  {manufacturers.map(manufacturer => {
                    return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                    {manufacturer.name}
                    </option>
                    );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={messageClasses} id="success-message">
                  Model Created Successfully!
                </div>
          </div>
        </div>
      </div>
    );
  }


export default ModelForm;
