import React, {useState, useEffect} from 'react';

function NewServiceForm({ getAppointments}) {
// Reason the useState hook to store "name" in the component's state,
// with a default initial value of an empty string.

    const [customer, setCustomer] = useState('');
    const [vin, setVin] = useState('');
    const [customers, setCustomers] = useState([]);
    const [time, setTime] = useState('');
    const [date, setDate] = useState('');
    const [reason, setReason] = useState("");
    const [technician, setTechnician] = useState("");
    const [technicians, setTechnicians] = useState([]);
    const [formSubmitted, setformSubmitted] = useState(false);
    const [vip, setVip] = useState(false);

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
      }
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
      }
    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
      }
    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
      }
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
      }
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
      }
      const handleVipChange = (event) => {
        const value = event.target.value;
        setVip(value);
      }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.customer = customer;
        data.vin = vin;
        data.time = time;
        data.date = date;
        data.reason = reason;
        data.technician_id = technician;
        data.vip = vip;
        data.has_finished = false;
        data.cancelled = false;

        console.log(data)

        const appointmentsUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(appointmentsUrl, fetchConfig);
        if (response.ok) {
          const newService = await response.json();

          console.log(newService);

          setCustomer('');
          setVin('');
          setTime('');
          setDate('');
          setReason('');
          setTechnician('');
          setVip('');
          setformSubmitted(true);

          return getAppointments();
        }
      }

      const fetchData = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const url1 = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);
        const response1 = await fetch(url1);

        if (response.ok) {
          const data = await response.json();
          setCustomers(data.customers)
        }

        if (response1.ok) {
          const data1 = await response1.json();
          setTechnicians(data1.technicians)
        }
      }


      useEffect(() => {
        fetchData();
      }, []);


      let messageClasses = 'alert alert-success d-none mb-0';
      let formClasses = '';

      if (formSubmitted) {
          messageClasses = 'alert alert-success mb-0';
          formClasses = 'd-none';
      }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new newservice</h1>
            <form className={formClasses} onSubmit={handleSubmit} id="create-newservice-form">


            {/* <div className="mb-3">
                <select onChange = {handleCustomerChange} value={customer} required id="customer" name="customer" className="form-select">
                  <option value="">choose a customer</option>
                  {customers.map(customer => {
                    return (
                    <option key={customer.id} value={customer.id}>
                    {customer.name}
                    </option>
                    );
                    })}
                </select>
              </div> */}
              <div className="form-floating mb-3">
                <input onChange = {handleCustomerChange} value={customer} placeholder="customer" required type="customer" name="customer" id="customer" className="form-control"/>
                <label htmlFor="customer">customer</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange = {handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">vin</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {handleTimeChange} value={time} placeholder="time" required type="time" name="time" id="time" className="form-control"/>
                <label htmlFor="time">time</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {handleDateChange} value={date} placeholder="date" required type="date" name="date" id="date" className="form-control"/>
                <label htmlFor="date">date</label>
              </div>
              <div className="mb-3">
                <label htmlFor="reason">Reason</label>
                <textarea onChange = {handleReasonChange} value={reason} className="form-control" id="reason" rows="3" name="reason" ></textarea>
              </div>
              <div className="form-floating mb-3">
                        <input onChange={handleVipChange} placeholder="VIP Status" required type="text" name="vip" id="vip_status" className="form-control" value={vip} />
                        <label htmlFor="vip_status">VIP Status</label>
                </div>
              <div className="mb-3">
                <select onChange = {handleTechnicianChange} value={technician} required id="technician" name="technician" className="form-select">
                  <option value="">choose a technician</option>
                  {technicians.map(technician => {
                    return (
                    <option key={technician.id} value={technician.id}>
                    {technician.name}
                    </option>
                    );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={messageClasses} id="success-message">
                Technician Created!
              </div>
          </div>
        </div>
      </div>
    );
  }
export default NewServiceForm;
