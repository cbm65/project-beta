import React, {useEffect, useState} from "react"
import { Link } from "react-router-dom";
function AppointmentList({ appointments, getAppointments}) {
  const deleteAppointment = async (id) => {
    fetch(`http://localhost:8100/api/appointments/${id}/`, {
      method:"delete",
    })
    .then(() => {
      return getAppointments()
    })
  }
  if (appointments === undefined) {
    return null
  }
  return (
  <>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Customer Name</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>
      <tbody>
        {appointments.map((appointment) => {
          return (
            <tr key={appointment.id}>
              <td>{ appointment.vin }</td>
              <td>{ appointment.customer }</td>
              <td>{ appointment.date }</td>
              <td>{ appointment.time }</td>
              <td>{ appointment.technician.name }</td>
              <td>{ appointment.reason }</td>
              <td>
                  <button type="button" value={appointment.id} onClick={() => deleteAppointment(appointment.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}
export default AppointmentList;
