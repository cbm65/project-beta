import React, {useEffect, useState} from "react"
import { Link } from "react-router-dom";


function AutomobileList({ automobiles, getAutomobiles}) {


  const deleteAutomobile = async (vin) => {
    fetch(`http://localhost:8100/api/automobiles/${vin}/`, {
      method:"delete",
    })
    .then(() => {
      return getAutomobiles()
    })
  }

  if (automobiles === undefined) {
    return null
  }


  return (
  <>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
        </tr>
      </thead>
      <tbody>
        {automobiles.map((automobile) => {
          return (
            <tr key={automobile.vin}>
              <td>{ automobile.vin }</td>
              <td>{ automobile.color }</td>
              <td>{ automobile.year }</td>
              <td>{ automobile.model.name }</td>
              <td>{ automobile.model.manufacturer.name }</td>
              <td>
                  <button type="button" value={automobile.vin} onClick={() => deleteAutomobile(automobile.vin)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}

export default AutomobileList;
