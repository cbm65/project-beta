import React, {useEffect, useState} from 'react';



function CustomerForm({ getCustomers}) {

// Set the useState hook to store "name" in the component's state,
// with a default initial value of an empty string.

    const [name, setName] = useState('');
    const [phone_number, setPhoneNumber] = useState('');
    const [address, setAddress] = useState('');
    const [formSubmitted, setformSubmitted] = useState(false);


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }

    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
      }

    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
      }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.phone_number = phone_number;
        data.address = address;


        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
          const newCustomer = await response.json();



          setName('');
          setPhoneNumber('');
          setAddress("");
          setformSubmitted(true);


          return getCustomers();
        }
      }

      let messageClasses = 'alert alert-success d-none mb-0';
      let formClasses = '';

      if (formSubmitted) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
      }


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a potential customer</h1>
            <form className={formClasses} onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                    <input onChange = {handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange = {handlePhoneNumberChange} value={phone_number} placeholder="phone_number" required type="text" phone_number="phone_number" id="phone_number" className="form-control"/>
                    <label htmlFor="phone_number">phone number</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange = {handleAddressChange} value={address} placeholder="address" required type="text" address="address" id="address" className="form-control"/>
                    <label htmlFor="address">address</label>
                </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={messageClasses} id="success-message">
                  Customer Added Successfully!
                </div>
          </div>
        </div>
      </div>
    );
  }


export default CustomerForm;
