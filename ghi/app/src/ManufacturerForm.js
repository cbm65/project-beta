import React, {useEffect, useState} from 'react';



function ManufacturerForm({ getManufacturers}) {

// Set the useState hook to store "name" in the component's state,
// with a default initial value of an empty string.

    const [name, setName] = useState('');
    const [formSubmitted, setformSubmitted] = useState(false);


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;


        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
          const newManufacturer = await response.json();

          setName('');
          setformSubmitted(true);

          return getManufacturers();
        }
      }

      let messageClasses = 'alert alert-success d-none mb-0';
      let formClasses = '';

      if (formSubmitted) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
      }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new manufacturer</h1>
            <form className={formClasses} onSubmit={handleSubmit} id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input onChange = {handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={messageClasses} id="success-message">
                  Manufacturer Created Successfully!
                </div>
          </div>
        </div>
      </div>
    );
  }


export default ManufacturerForm;
