from django.urls import path

from .views import (
    api_representative,
    api_representatives,
    api_customers,
    api_customer,
    api_sales,
    api_sale,
)

urlpatterns = [
    path(
        "representatives/",
        api_representatives,
        name="api_representatives",
    ),
    path(
        "representatives/<int:pk>/",
        api_representative,
        name="api_representative",
    ),
    path(
        "customers/",
        api_customers,
        name="api_customers",
    ),
    path(
        "customers/<int:pk>/",
        api_customer,
        name="api_customer",
    ),
    path(
        "sales/",
        api_sales,
        name="api_sales",
    ),
    path(
        "sales/<int:pk>/",
        api_sale,
        name="api_sale",
    ),
]
