from common.json import ModelEncoder

from .models import Sale, Customer, Representative, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "phone_number",
        "address",
    ]


class RepresentativeEncoder(ModelEncoder):
    model = Representative
    properties = [
        "id",
        "name",
        "employee_number",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "automobile",
        "customer",
        "representative",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerEncoder(),
        "representative": RepresentativeEncoder(),
    }
