from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=200)
    sold = models.BooleanField(default=False)

class Representative(models.Model):

    name = models.CharField(max_length=200)
    employee_number = models.PositiveBigIntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_representative", kwargs={"pk": self.id})



class Customer(models.Model):

    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"pk": self.id})

class Sale(models.Model):

    price = models.PositiveBigIntegerField()

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.PROTECT,
    )

    representative = models.ForeignKey(
        Representative,
        related_name="representatives",
        on_delete=models.PROTECT,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete=models.PROTECT,
    )



    def get_api_url(self):
        return reverse("api_show_sale", kwargs={"pk": self.pk})
