from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_id = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=200)


class Representative(models.Model):

    name = models.CharField(max_length=200)
    employee_number = models.PositiveBigIntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_representative", kwargs={"pk": self.pk})


class Customer(models.Model):

    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"pk": self.pk})

class Sale(models.Model):

    price = models.DecimalField(max_digits=6, decimal_places=2)

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    representative = models.ForeignKey(
        Representative,
        related_name="representatives",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.price

    def get_api_url(self):
        return reverse("api_show_sale", kwargs={"pk": self.pk})
